//
//  Sun.cpp
//  Lost Sunrise
//
//  Created by Roman on 5/7/13.
//
//

#include "Sun.h"
#include "Constants.h"
#include "MapHelper.h"
#include "MyMap.h"

Sun::Sun()
{

}

Sun::~Sun()
{
 
}

bool Sun::init(b2World *world, CCDictionary *properties)
{
    CCSpriteFrameCache* cache = CCSpriteFrameCache::sharedSpriteFrameCache();
    cache->addSpriteFramesWithFile("Scenes/GameScene/SunSpriteSheet.plist");
    
    CCPoint pos = MapHelper::positionWithProperties(properties);
    if (CCSprite::initWithSpriteFrameName("body.png"))
    {
        canThrow = true;
        crystalDelegate = NULL;
        setTag(ObjectTypeSun);
        CCSprite *sun = CCSprite::createWithSpriteFrameName("sun.png");
        sun->cocos2d::CCNode::setPosition(boundingBox().size.width/2, boundingBox().size.height/2);
        addChild(sun, -1);
        
        CCScaleBy *scale = CCScaleBy::create(0.5, 1.2);
        CCSequence *sequence = CCSequence::create(scale, scale->reverse(), NULL);
        sun->runAction(CCRepeatForever::create(sequence));
        
        //eye
        eyes = CCSprite::createWithSpriteFrameName("Eyes/sun_eyes_00.png");
        eyes->cocos2d::CCNode::setPosition(boundingBox().size.width/2, boundingBox().size.height/2);
        addChild(eyes);
        
        CCArray* animFrames = CCArray::createWithCapacity(10);
        
        //char str[100] = {0};
        
        for(int i = 0; i < 9; i++)
        {
            //Eyes/sun_eyes_00.png
            CCString *name  = CCString::createWithFormat("Eyes/sun_eyes_%02d.png",i);
            //sprintf(str, "grossini_dance_%02d.png", i);
            CCSpriteFrame* frame = cache->spriteFrameByName( name->getCString());
            animFrames->addObject(frame);
        }

        blinkAnimate = CCAnimate::create(CCAnimation::createWithSpriteFrames(animFrames, 0.06f));
        //
        CCSprite *mouth = CCSprite::createWithSpriteFrameName("Mouth/sun_mouth_00.png");
        mouth->setPosition(ccp(boundingBox().size.width/2, boundingBox().size.height/2));

        addChild(mouth);
        
        setScale(0.3);
        setPosition(pos);
        
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        bodyDef.position.Set(pos.x/PTM_RATIO, pos.y/PTM_RATIO);
        
        body = world->CreateBody(&bodyDef);
        
        // Define another box shape for our dynamic body.
        b2CircleShape *circleShape = new b2CircleShape();
        circleShape->m_radius = ((boundingBox().size.width/2) / PTM_RATIO) * getScale();
        
        // Define the dynamic body fixture.
        b2FixtureDef fixtureDef;
        fixtureDef.shape = circleShape;
        fixtureDef.density = 1.0f;
        fixtureDef.friction = 0.01f;
        fixtureDef.restitution = 1.0f;
        fixtureDef.filter.categoryBits = CATEGORY_SUN;
        fixtureDef.filter.maskBits = MASK_SUN;
        
        delete circleShape;
        circleShape = NULL;
        
        body->CreateFixture(&fixtureDef);
        body->SetUserData(this);
        
        dieParticle = CCParticleSystemQuad::create("particle/sunDie.plist");

        blink();
        return true;
    }
    return false;
}

void Sun::die()
{
    CCScaleTo *scale = CCScaleTo::create(1.0, 0.0);
    CCCallFunc *explosion = CCCallFunc::create(this, callfunc_selector(Sun::explosion));
    runAction(CCSequence::create(scale, explosion, NULL));
}

void Sun::explosion()
{
    dieParticle->setPosition(getPosition());
    getParent()->getParent()->addChild(dieParticle);
    dieParticle->resetSystem();
}

void Sun::moveToPos(b2Vec2 pos)
{
    body->SetLinearVelocity(b2Vec2((pos.x - body->GetPosition().x) * 2, (pos.y - body->GetPosition().y) * 2));
    body->SetAngularVelocity(0);
}

void Sun::setPosition(const cocos2d::CCPoint &position)
{
    CCSprite::setPosition(position);
    CCNode *node = getParent();
    if (node != NULL)
    {
        MyMap *map = (MyMap*) node->getParent();
        if (map != NULL) map->sunPosition = position;
    }
}

void Sun::setScale(float fScale)
{
    CCSprite::setScale(fScale);
    
    if (fScale < 0.15)
        gameDelegate->gameOver();
}

void Sun::blink()
{
    float time = 2 + (arc4random() % 400) / 100;
    CCDelayTime *timeWait = CCDelayTime::create(time);
    CCCallFunc *nextBlink = CCCallFunc::create(this, callfunc_selector(Sun::blink));
    //runAction(CCSequence::createWithTwoActions(timeWait, nextBlink));
    
    eyes->runAction(CCSequence::create(blinkAnimate, timeWait, nextBlink, NULL));
}
