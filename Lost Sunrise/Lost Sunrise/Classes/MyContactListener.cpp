//
//  MyContactListener.m
//  Box2DPong
//
//  Created by Ray Wenderlich on 2/18/10.
//  Copyright 2010 Ray Wenderlich. All rights reserved.
//

#include "MyContactListener.h"
MyContactListener::MyContactListener() : _contacts() {
}

MyContactListener::~MyContactListener() {
}

void MyContactListener::BeginContact(b2Contact* contact) {
    // We need to copy out the data because the b2Contact passed in
    // is reused.
    b2WorldManifold worldManifold;
    contact->GetWorldManifold(&worldManifold);
    
    MyContact myContact = { contact->GetFixtureA(), contact->GetFixtureB(), worldManifold};
    _contacts.push_back(myContact);
}

void MyContactListener::EndContact(b2Contact* contact) {
    b2WorldManifold worldManifold;
    contact->GetWorldManifold(&worldManifold);
    
    MyContact myContact = { contact->GetFixtureA(), contact->GetFixtureB(), worldManifold};
    std::vector<MyContact>::iterator pos;
    pos = std::find(_contacts.begin(), _contacts.end(), myContact);
    if (pos != _contacts.end()) {
        _contacts.erase(pos);
    }
}

void MyContactListener::PreSolve(b2Contact* contact, const b2Manifold* oldManifold) {
}

void MyContactListener::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse) {
}

