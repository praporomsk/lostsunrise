//
//  GameManager.cpp
//  Lost Sunrise
//
//  Created by Roman on 5/7/13.
//
//

#include "GameManager.h"
#include "GameScene.h"

GameManager *GameManager::m_singleton = NULL;

GameManager::GameManager()
{
    
}

GameManager* GameManager::sharedGameManager()
{
    if (NULL == m_singleton)
    {
        m_singleton = new GameManager();
        m_singleton->currentLevelPack = 1;
        m_singleton->currentLevel = 1;
    }
    return  m_singleton;
}

void GameManager::restartLevel()
{
    CCDirector::sharedDirector()->replaceScene(GameScene::scene());
}

void GameManager::nextLevel()
{
    
}

void GameManager::launchMainMenu()
{

}

void GameManager::launchSelectLevel()
{
    CCLOG("launchSelectLevel");
}