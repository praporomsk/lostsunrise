//
//  Crystal.h
//  Lost Sunrise
//
//  Created by Roman on 5/28/13.
//
//

#ifndef __Lost_Sunrise__Crystal__
#define __Lost_Sunrise__Crystal__

#include "cocos2d.h"
#include "Box2D.h"
#include "Sun.h"
#include "CrystalDelegate.h"

USING_NS_CC;

class Crystal : public CCSprite, public CrystalDelegate
{
public:
    ~Crystal();
    Crystal();
    
    bool init(b2World* world, CCDictionary *properties);
    b2Body *body;
    bool isActive;
    bool canCatch;
    void setIsActive();
    virtual void deActive();
};

#endif /* defined(__Lost_Sunrise__Crystal__) */
