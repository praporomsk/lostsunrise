//
//  Platform.h
//  Lost Sunrise
//
//  Created by Roman on 5/8/13.
//
//

#ifndef __Lost_Sunrise__Platform__
#define __Lost_Sunrise__Platform__

#include "cocos2d.h"
#include "Box2D.h"

USING_NS_CC;

class Platform : public CCNode
{
public:
    ~Platform();
    Platform();
    
    bool init(b2World* world, CCDictionary *properties);
    b2Body *body;
    float reflectance;
};
#endif /* defined(__Lost_Sunrise__Platform__) */
