//
//  Cloud.h
//  Lost Sunrise
//
//  Created by Roman on 5/20/13.
//
//

#ifndef __Lost_Sunrise__Cloud__
#define __Lost_Sunrise__Cloud__

#include "cocos2d.h"
#include "Box2D.h"

USING_NS_CC;

class Cloud : public CCSprite
{
public:
    ~Cloud();
    Cloud();
    
    bool init(CCDictionary *properties);
    bool withRain;
    b2Body *body;

    void setParent(cocos2d::CCNode *parent);
    void setPosition(const cocos2d::CCPoint &pos);

private:
    CCParticleSystemQuad *m_rainParticle;
};
#endif /* defined(__Lost_Sunrise__Cloud__) */
