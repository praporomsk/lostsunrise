//
//  CrystalDelegate.h
//  Lost Sunrise
//
//  Created by Roman on 5/28/13.
//
//

#ifndef Lost_Sunrise_CrystalDelegate_h
#define Lost_Sunrise_CrystalDelegate_h

class CrystalDelegate
{
public:
    CrystalDelegate() {};
    virtual ~CrystalDelegate() {};
    
    virtual void deActive() {};
};

#endif
