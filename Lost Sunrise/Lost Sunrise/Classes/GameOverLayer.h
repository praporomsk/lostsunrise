//
//  GameOverLayer.h
//  Lost Sunrise
//
//  Created by Roman on 8/11/13.
//
//

#ifndef __Lost_Sunrise__GameOverLayer__
#define __Lost_Sunrise__GameOverLayer__


#include "cocos2d.h"

USING_NS_CC;

class GameOverLayer :public CCLayer
{
public:
    GameOverLayer();
    ~GameOverLayer();
    bool init();
    void winAnimation();
    void showGameOverMenu();
//private:

    
private:
    CCSprite *m_background;
    CCSprite *m_moon;
    
    CCPoint starPos(int number);
    void showWinMenu();
    void showStars();
    
    void selectLevel(CCMenuItem *sender);
    void restartLevel(CCMenuItem *sender);
    void nextLevel(CCMenuItem *sender);
};

#endif /* defined(__Lost_Sunrise__GameOverLayer__) */
