//
//  Star.h
//  Lost Sunrise
//
//  Created by Roman on 5/18/13.
//
//

#ifndef __Lost_Sunrise__Star__
#define __Lost_Sunrise__Star__

#include "cocos2d.h"
#include "Box2D.h"

USING_NS_CC;

class Star : public CCSprite
{
public:
    ~Star();
    Star();
    
    bool init(b2World* world, CCDictionary *properties);
    b2Body *body;
    
};

#endif /* defined(__Lost_Sunrise__Star__) */
