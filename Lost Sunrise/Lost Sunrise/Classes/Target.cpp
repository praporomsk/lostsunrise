//
//  Target.cpp
//  Lost Sunrise
//
//  Created by Roman on 6/30/13.
//
//

#include "Target.h"
#include "Constants.h"

Target::Target()
{
    
}

Target::~Target()
{
    m_targetPoints->removeAllObjects();
    delete m_targetPoints;
    m_targetPoints = NULL;
}

bool Target::init(b2World *world, float radius)
{
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    m_body = world->CreateBody(&bodyDef);

    b2CircleShape *circleShape = new b2CircleShape();
    circleShape->m_radius = radius;
    
    b2FixtureDef fixtureDef;
    fixtureDef.shape = circleShape;
    fixtureDef.density = 1.0f;
    fixtureDef.friction = 0.01f;
    fixtureDef.restitution = 1.0f;
    fixtureDef.filter.categoryBits = CATEGORY_TARGET;
    fixtureDef.filter.maskBits = MASK_TARGET;
    
    delete circleShape;
    circleShape = NULL;
    
    m_body->CreateFixture(&fixtureDef);
    m_body->SetUserData(NULL);
    m_targetPoints = new CCArray();
    m_targetPoints->initWithCapacity(15);

    for (int i = 0; i < 15; i++)
    {
        CCSprite *target = CCSprite::createWithSpriteFrameName("target.png");
        target->setVisible(false);
        m_targetPoints->addObject(target);
    }
    
    return true;
}

void Target::setVisible(bool isVisible)
{
    CCObject* obj;
    CCARRAY_FOREACH(m_targetPoints,obj)
    {
        CCSprite *target = (CCSprite*)obj;
        target->setVisible(isVisible);
    }
}

void Target::updateTarget(b2World *world)
{
    CCPoint force = ccpForAngle(angle);
    force = ccp(force.x, -force.y);
    force = ccpMult(force, 100);
    
    m_body->SetLinearVelocity(b2Vec2(0.0f, 0.0f));
    m_body->ApplyForceToCenter(b2Vec2(force.x, force.y));
    
    int velocityIterations = 8;
    int positionIterations = 1;
    
    CCObject* obj;
    CCARRAY_FOREACH(m_targetPoints,obj)
    {
        world->Step(0.1, velocityIterations, positionIterations);
        CCSprite *target = dynamic_cast<CCSprite*>(obj);
        target->setVisible(true);
        target->setPosition(ccp(m_body->GetPosition().x * PTM_RATIO, m_body->GetPosition().y * PTM_RATIO));
    }
}