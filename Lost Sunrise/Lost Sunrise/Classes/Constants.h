//
//  Constants.h
//  Lost Sunrise
//
//  Created by Roman on 5/7/13.
//
//

#ifndef __Lost_Sunrise__Constants__
#define __Lost_Sunrise__Constants__

#define PTM_RATIO 32

enum _entityCategory {
    CATEGORY_SUN =          0x0001,
    CATEGORY_TARGET =       0x0002,
    CATEGORY_PLATFORM =     0x0004,
    CATEGORY_SCENERY_OBJECTS =      0x0008,
};

enum _entityMask {
    MASK_SUN = CATEGORY_PLATFORM | CATEGORY_SCENERY_OBJECTS,
    MASK_TARGET = CATEGORY_PLATFORM,
    MASK_PLATFORM = -1,
    MASK_SCENERY_OBJECTS = CATEGORY_SUN,
};


typedef enum {
    ObjectTypeSun,
    ObjectTypePlatform,
    ObjectTypeStar,
    ObjectTypeCloud,
    ObjectTypeCrystal,
    
    NeedDestroyBody,
} ObjectType;

#endif /* defined(__Lost_Sunrise__Constants__) */
