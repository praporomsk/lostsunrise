//
//  Sun.h
//  Lost Sunrise
//
//  Created by Roman on 5/7/13.
//
//

#ifndef __Lost_Sunrise__Sun__
#define __Lost_Sunrise__Sun__

#include "cocos2d.h"
#include "Box2D.h"
#include "CrystalDelegate.h"
#include "GameDelegate.h"

USING_NS_CC;

class Sun : public CCSprite
{
    
private:
    void blink();
    
    CCSprite *eyes;
    CCAnimate *blinkAnimate;
public:
    ~Sun();
    Sun();
    
    bool init(b2World* world, CCDictionary *properties);
    b2Body *body;
    bool canThrow;

    void die();
    void explosion();
    void setPosition(const cocos2d::CCPoint &position);
    void setScale(float fScale);
    
    void moveToPos(b2Vec2 pos);
    CrystalDelegate *crystalDelegate;
    GameDelegate *gameDelegate;
    CCParticleSystemQuad *dieParticle;
    CCParticleSystemQuad *sunshineParticle;
};

#endif /* defined(__Lost_Sunrise__Sun__) */
