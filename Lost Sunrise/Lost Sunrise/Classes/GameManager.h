//
//  GameManager.h
//  Lost Sunrise
//
//  Created by Roman on 5/7/13.
//
//

#ifndef __Lost_Sunrise__GameManager__
#define __Lost_Sunrise__GameManager__

#include "cocos2d.h"

class GameManager
{
private:
    GameManager();
    static GameManager* m_singleton;
public:
    int currentLevelPack;
    int currentLevel;
    
    static GameManager* sharedGameManager();

    void restartLevel();
    void nextLevel();
    
    void launchMainMenu();
    void launchSelectLevel();
};
#endif /* defined(__Lost_Sunrise__GameManager__) */
