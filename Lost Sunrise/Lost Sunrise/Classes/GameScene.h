//
//  GameScene.h
//  Lost Sunrise
//
//  Created by Roman on 5/7/13.
//
//

#ifndef __Lost_Sunrise__GameScene__
#define __Lost_Sunrise__GameScene__

#include "cocos2d.h"
#include "Box2D.h"
#include "Sun.h"
#include "MyContactListener.h"
#include "MyMap.h"
#include "Target.h"
#include "GameDelegate.h"
#include "GameOverLayer.h"

USING_NS_CC;

class GameScene : public CCLayer, public GameDelegate
{
public:
    ~GameScene();
    GameScene();
    
    static CCScene* scene();
    void initPhysics();
    void addBackground();
    void addTarget();
    
    void parseTileMap();
    void addObject(CCDictionary *properties);
    void update(float dt);
    
    void youWin();
    void winAnimation();
    
    void gameOver();
    void gameOverAnimation();
    void showGameOverMenu();
    
    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesMoved(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
private:
    b2World* m_world;
    MyMap *m_map;
    CCSpriteBatchNode *m_gameBatchNode;
    GameOverLayer *m_gameOverLayer;
    
    Target *m_target;
    Sun *m_sun;
    
    CCArray *m_clouds;
    MyContactListener *m_contactListener;
    bool touchSun;
    float oldPosY;
};

#endif /* defined(__Lost_Sunrise__GameScene__) */
