//
//  GameDelegate.h
//  Lost Sunrise
//
//  Created by Roman on 7/21/13.
//
//

#ifndef Lost_Sunrise_GameDelegate_h
#define Lost_Sunrise_GameDelegate_h

class GameDelegate
{
public:
    GameDelegate() {};
    virtual ~GameDelegate() {};
    
    virtual void gameOver() {};
};

#endif
