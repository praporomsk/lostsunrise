//
//  MyMap.h
//  Lost Sunrise
//
//  Created by Roman on 5/30/13.
//
//

#ifndef __Lost_Sunrise__MyMap__
#define __Lost_Sunrise__MyMap__

#include "cocos2d.h"
#include "Box2D.h"

USING_NS_CC;

typedef enum {
    MAP_MODE_TOUCH_SCROLL,
    MAP_MODE_SCROLL_TO_STOP,
    MAP_MODE_FOLLOW,
} MAP_MODE;

class MyMap : public CCTMXTiledMap
{
public:
    ~MyMap();
    MyMap();
    
    bool initWithTMXFile(const char *tmxFile);
    void update(float delta);
    void setPosition(const cocos2d::CCPoint &position);

    float getRealHeight();
    float speed;
    MAP_MODE mapMode;
    CCPoint sunPosition;
};
#endif /* defined(__Lost_Sunrise__MyMap__) */
