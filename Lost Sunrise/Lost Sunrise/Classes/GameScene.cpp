//
//  GameScene.cpp
//  Lost Sunrise
//
//  Created by Roman on 5/7/13.
//
//

#include "GameScene.h"
#include "Constants.h"
#include "GameManager.h"
#include "Sun.h"
#include "Platform.h"
#include "Star.h"
#include "Cloud.h"
#include "Crystal.h"

#define ifDynamicCast(__type__,__var__,__varName__) \
__type__ __varName__ = dynamic_cast<__type__>(__var__); \
if( __varName__ )

#pragma mark -
#pragma mark init and delete 
CCScene* GameScene::scene()
{
    CCScene *scene = CCScene::create();
    CCLayer* layer = new GameScene();
    scene->addChild(layer);
    layer->release();
    return scene;
}

GameScene::GameScene()
{
    setTouchEnabled(true);
    initPhysics();
    m_clouds = new CCArray();
    m_clouds->init();
    
    addBackground();
    addTarget();
    scheduleUpdate();
}

GameScene::~GameScene()
{
    delete m_target;
    m_target = NULL;
    
    delete m_clouds;
    m_clouds = NULL;
    
    delete m_world;
    m_world = NULL;
    
    delete m_contactListener;
    m_contactListener = NULL;
}

void GameScene::initPhysics()
{
    touchSun = false;
    CCSize s = CCDirector::sharedDirector()->getWinSize();
    b2Vec2 gravity;
    gravity.Set(0.0f, 0.0f);
    m_world = new b2World(gravity);
    m_world->SetAllowSleeping(true);
    m_world->SetContinuousPhysics(true);
    
    uint32 flags = 0;
    flags += b2Draw::e_shapeBit;

    // Define the ground body.
    b2BodyDef groundBodyDef;
    groundBodyDef.position.Set(0, 0); // bottom-left corner
    
    b2Body* groundBody = m_world->CreateBody(&groundBodyDef);
    // Define the ground box shape.
    b2EdgeShape groundBox;
    
    // bottom
    groundBox.Set(b2Vec2(0,0), b2Vec2(s.width/PTM_RATIO,0));
    groundBody->CreateFixture(&groundBox,0);
    
// Create contact listener
    m_contactListener = new MyContactListener();
    m_world->SetContactListener(m_contactListener);
}
#pragma mark -
#pragma mark add objects

void GameScene::addBackground()
{
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("Scenes/GameScene/GameSceneSpriteSheet.plist");
    m_gameBatchNode = CCSpriteBatchNode::create("Scenes/GameScene/GameSceneSpriteSheet.png");
    
    int pack = GameManager::sharedGameManager()->currentLevelPack;
    int level = GameManager::sharedGameManager()->currentLevel;
    CCString *mapName = CCString::createWithFormat("Scenes/GameScene/Levels/level_%02d_%02d.tmx", pack, level);
    m_map = new MyMap();
    m_map->initWithTMXFile(mapName->getCString());
    m_map->autorelease();
    this->addChild(m_map);
    m_map->addChild(m_gameBatchNode, 3);
    
    m_gameOverLayer = new GameOverLayer();
    m_gameOverLayer->init();
    m_gameOverLayer->autorelease();
    m_gameOverLayer->setPosition(ccp(0.0f, m_map->getRealHeight()));
    m_map->addChild(m_gameOverLayer, -1);
    
    parseTileMap();
}

void GameScene::addTarget()
{
    float radius = ((m_sun->boundingBox().size.width/2) / PTM_RATIO) * m_sun->getScale();
    m_target = new Target();
    m_target->init(m_world, radius);
    
    CCObject* obj;
    CCARRAY_FOREACH(m_target->m_targetPoints,obj)
    {
        m_gameBatchNode->addChild((CCSprite*)obj, 10);
    }
}

#pragma mark -
#pragma mark other
void GameScene::parseTileMap()
{
    CCArray * groups = m_map->getObjectGroups();
    for( int i = 0; i < groups->count(); i++ )
    {
        ifDynamicCast(CCTMXObjectGroup*, groups->objectAtIndex(i), group)
        {
            auto objects = group->getObjects();
            for (int j = 0; j < objects->count(); j++)
            {
                ifDynamicCast(CCDictionary*, objects->objectAtIndex(j), properties)
                {
                    addObject(properties);
                }
            }
        }
    }
}

void GameScene::addObject(CCDictionary *properties)
{
    const CCString * type = properties->valueForKey("type");
    if (type->compare("Platform") == 0)
    {
        Platform *platform = new Platform();
        platform->init(m_world, properties);
        platform->autorelease();
        addChild(platform);
    }else if(type->compare("sunSpawn") == 0){
        CCSpriteBatchNode* sunbatch = CCSpriteBatchNode::create("Scenes/GameScene/SunSpriteSheet.png");
        m_map->addChild(sunbatch, 2);
        
        m_sun = new Sun();
        m_sun->init(m_world, properties);
        m_sun->autorelease();
        sunbatch->addChild(m_sun);
        m_sun->gameDelegate = this;
    }else if (type->compare("star") == 0){
        Star *star = new Star();
        star->init(m_world, properties);
        star->autorelease();
        m_gameBatchNode->addChild(star);
    }else if (type->compare("cloud") == 0){
        Cloud *cloud = new Cloud();
        cloud->init(properties);
        cloud->autorelease();
        m_clouds->addObject(cloud);
        m_map->addChild(cloud);
    }else if (type->compare("crystal") == 0){
        Crystal *crystal = new Crystal();
        crystal->init(m_world, properties);
        crystal->autorelease();
        m_gameBatchNode->addChild(crystal);
    }
}

void GameScene::youWin()
{
    CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
    CCMoveTo *sunMove = CCMoveTo::create(2.0, ccp(screenSize.width/2, m_map->boundingBox().size.height + screenSize.height /2));
    CCScaleTo *scale = CCScaleTo::create(1.0, 1.0);
    CCRotateTo *rotation = CCRotateTo::create(2.5, 0.0);
    CCSpawn *sunSpawn = CCSpawn::create(sunMove, scale, rotation, NULL);
    m_sun->runAction(sunSpawn);

    CCCallFunc *winAnimation = CCCallFunc::create(this, callfunc_selector(GameScene::winAnimation));
    CCMoveTo *mapMove = CCMoveTo::create(3.0, ccp(0.0, -m_map->boundingBox().size.height));
    m_map->runAction(CCSequence::create(mapMove, winAnimation, NULL));
}

void GameScene::winAnimation()
{
    m_gameOverLayer->winAnimation();
}

void GameScene::gameOver()
{
    unscheduleUpdate();
    m_sun->die();
    m_map->unscheduleUpdate();
    setTouchEnabled(false);
    CCDelayTime *time = CCDelayTime::create(2.0);
    CCCallFunc *gameOverAnimation = CCCallFunc::create(this, callfunc_selector(GameScene::gameOverAnimation));
    runAction(CCSequence::create(time, gameOverAnimation, NULL));
}

void GameScene::gameOverAnimation()
{
    CCMoveTo *mapMove = CCMoveTo::create(2.0, ccp(0.0, -m_map->boundingBox().size.height));
    CCEaseSineInOut *ease = CCEaseSineInOut::create(mapMove);
    CCCallFunc *showGameOverMenu = CCCallFunc::create(this, callfunc_selector(GameScene::showGameOverMenu));
    m_map->runAction(CCSequence::createWithTwoActions(ease, showGameOverMenu));
}

void GameScene::showGameOverMenu()
{
    m_gameOverLayer->showGameOverMenu();
}
#pragma mark -
#pragma mark tick metods
void GameScene::update(float dt)
{
    //It is recommended that a fixed time step is used with Box2D for stability
    //of the simulation, however, we are using a variable time step here.
    //You need to make an informed choice, the following URL is useful
    //http://gafferongames.com/game-physics/fix-your-timestep/
    
    int velocityIterations = 8;
    int positionIterations = 1;
    
    // Instruct the world to perform a single step of simulation. It is
    // generally best to keep the time step and iterations fixed.
    m_world->Step(dt, velocityIterations, positionIterations);
    
    //Iterate over the bodies in the physics world
    for (b2Body* b = m_world->GetBodyList(); b; b = b->GetNext())
    {
        if (b->GetUserData() != NULL) {
            //Synchronize the AtlasSprites position and rotation with the corresponding body
            CCSprite* myActor = (CCSprite*)b->GetUserData();
            myActor->setPosition( CCPointMake( b->GetPosition().x * PTM_RATIO, b->GetPosition().y * PTM_RATIO) );
            myActor->setRotation( -1 * CC_RADIANS_TO_DEGREES(b->GetAngle()) );
            if (myActor->getTag() == NeedDestroyBody)
            {
                m_world->DestroyBody(b);
            }
        }
    }
    
    std::vector<MyContact>::iterator pos;
    for(pos = m_contactListener->_contacts.begin(); pos != m_contactListener->_contacts.end(); ++pos) {
        MyContact contact = *pos;
        
        b2Body *bodyA = contact.fixtureA->GetBody();
        b2Body *bodyB = contact.fixtureB->GetBody();
        if (bodyA->GetUserData() != NULL && bodyB->GetUserData() != NULL) {
            CCSprite *spriteA = (CCSprite *) bodyA->GetUserData();
            b2Body *sunBody;
            b2Body *objectBody;
            if (spriteA->getTag() == ObjectTypeSun)
            {
                sunBody = bodyA;
                objectBody = bodyB;
            }else{
                sunBody = bodyB;
                objectBody = bodyA;
            }
            
            Sun *sun = (Sun *) sunBody->GetUserData();
            CCSprite *object = (CCSprite*) objectBody->GetUserData();
            
            if (object->getTag() == ObjectTypePlatform) {
                Platform *platform = (Platform*) object;
                sun->setScale(sun->getScale() - platform->reflectance * 0.3);
            }else if (object->getTag() == ObjectTypeStar){
                m_world->DestroyBody(objectBody);
                object->removeFromParentAndCleanup(true);
                return;
            }else if (object->getTag() == ObjectTypeCrystal){
                Crystal *crystal = (Crystal*) object;
                if (crystal->canCatch)
                {
                    if (ccpDistance(m_sun->getPosition(), crystal->getPosition()) < 2){
                        crystal->setTag(NeedDestroyBody);
                        m_sun->body->SetLinearVelocity(b2Vec2(0.0f, 0.0f));
                        m_sun->canThrow = true;
                    }else{
                        sun->moveToPos(crystal->body->GetPosition());
                        sun->crystalDelegate = crystal;
                    }
                    crystal->setIsActive();
                }
            }
        }
    }

//check for victory
    if (m_sun->getPosition().y > m_map->getRealHeight())
    {
        unscheduleUpdate();
        youWin();
        setTouchEnabled(false);
    }

    CCObject* object;
    CCARRAY_FOREACH(m_clouds,object) {
        Cloud* cloud = (Cloud*)object;
        if (cloud->boundingBox().containsPoint(m_sun->getPosition()))
        {
            m_sun->setScale(m_sun->getScale() - 0.01);
        }
    }
}

#pragma mark -
#pragma mark touch metods
void GameScene::ccTouchesBegan(CCSet* touches, CCEvent* event)
{
    CCSetIterator it;
    CCTouch* touch;
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        if(!touch)
            break;
        
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        
        CCPoint pos = ccpSub(location, m_map->getPosition());
      
        if (m_sun->boundingBox().containsPoint(pos) && m_sun->canThrow)
        {
            m_map->mapMode = MAP_MODE_FOLLOW;
            touchSun = true;
        }else{
            m_map->mapMode = MAP_MODE_TOUCH_SCROLL;
            oldPosY = location.y;
            m_map->speed = 0;
        }
    }
}

void GameScene::ccTouchesMoved(CCSet* touches, CCEvent* event)
{
    CCSetIterator it;
    CCTouch* touch;
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        if(!touch)
            break;
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        
        if (touchSun)
        {
            
            CCPoint pos = ccpSub(location, m_map->getPosition());
            
            m_target->angle = M_PI -ccpToAngle(ccpSub(pos, m_sun->getPosition()));
            m_target->m_body->SetTransform(m_sun->body->GetPosition(), m_sun->body->GetAngle());
            m_target->updateTarget(m_world);
        }else{
            m_map->speed = location.y - oldPosY;
            m_map->setPosition(ccp(m_map->getPosition().x, m_map->getPosition().y + m_map->speed));
            oldPosY = location.y;
        }
    }
}

void GameScene::ccTouchesEnded(CCSet* touches, CCEvent* event)
{
    if (touchSun)
    {
        touchSun = false;
        //m_arrow->setVisible(false);
        CCPoint force = ccpForAngle(m_target->angle );
        force = ccp(force.x, -force.y);
        force = ccpMult(force, 300);
        
        m_target->setVisible(false);
        m_sun->body->ApplyForceToCenter(b2Vec2(force.x, force.y));
        m_sun->canThrow = false;
        if (m_sun->crystalDelegate != NULL)
        {
            m_sun->crystalDelegate->deActive();
        }
    }else{
        m_map->mapMode = MAP_MODE_SCROLL_TO_STOP;
    }
}