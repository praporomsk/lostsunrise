//
//  Target.h
//  Lost Sunrise
//
//  Created by Roman on 6/30/13.
//
//

#ifndef __Lost_Sunrise__Target__
#define __Lost_Sunrise__Target__

#include "cocos2d.h"
#include "Box2D.h"

USING_NS_CC;

class Target : public CCObject
{
    
public:
    ~Target();
    Target();
    
    virtual bool init(b2World* world,float radius);
    void updateTarget(b2World *world);
    void setVisible(bool isVisible);
    
    float angle;
    b2Body *m_body;

    CCArray *m_targetPoints;
};
#endif /* defined(__Lost_Sunrise__Target__) */
