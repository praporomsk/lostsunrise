//
//  Cloud.cpp
//  Lost Sunrise
//
//  Created by Roman on 5/20/13.
//
//

#include "Cloud.h"
#include "Constants.h"
#include "MapHelper.h"
Cloud::Cloud()
{
    
}

Cloud::~Cloud()
{
    
}

bool Cloud::init(CCDictionary *properties)
{
    CCPointArray *points = new CCPointArray();
    points->initWithCapacity(1);
    points->autorelease();
    points = MapHelper::parseDictionary(properties);
    
    CCPoint pos = MapHelper::positionWithProperties(properties);
    if (CCSprite::initWithSpriteFrameName("enemy.png"))
    {
        setTag(ObjectTypeCloud);
        m_rainParticle = NULL;
        setPosition(pos);
        
        //animation
        CCPoint movePos = points->getControlPointAtIndex(1);
        
        float speed = 15 * properties->valueForKey("speed")->floatValue();
        float time = ccpDistance(getPosition(), movePos) / speed;
        CCLOG("time =%f",time);
        CCMoveTo *move = CCMoveTo::create(time, movePos);
        CCMoveTo *back = CCMoveTo::create(time, getPosition());
        CCSequence *sequence = CCSequence::create(move, back, NULL);
        runAction(CCRepeatForever::create(sequence));

        withRain = false;
        if (properties->valueForKey("rain")->floatValue() == 1)
            withRain = true;

        return true;
    }
    return false;
}

void Cloud::setPosition(const cocos2d::CCPoint &pos)
{
    CCSprite::setPosition(pos);
    if (m_rainParticle != NULL)
        m_rainParticle->setSourcePosition(pos);

}

void Cloud::setParent(cocos2d::CCNode *parent)
{
    CCSprite::setParent(parent);
    if (withRain)
    {
        m_rainParticle = CCParticleSystemQuad::create("particle/raining.plist");
        m_rainParticle->setPosition(ccp(0.0, -boundingBox().size.height * 0.3));
        m_rainParticle->setPositionType(kCCPositionTypeGrouped);
        parent->addChild(m_rainParticle);
    }
}