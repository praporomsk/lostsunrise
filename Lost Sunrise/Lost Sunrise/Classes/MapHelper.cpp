//
//  MapHelper.cpp
//  Rabbit
//
//  Created by Roman on 4/18/13.
//
//

#include "MapHelper.h"

CCPointArray* MapHelper::parseDictionary(CCDictionary* properties)
{
    CCPoint startPos = ccp(properties->valueForKey("x")->floatValue(), properties->valueForKey("y")->floatValue());
    startPos = ccpMult(startPos, 1/ CCDirector::sharedDirector()->getContentScaleFactor());
    
    CCPointArray *points = new CCPointArray();
    points->initWithCapacity(1);
    points->autorelease();
    points->addControlPoint(startPos);
    
    const CCString * linePoints = properties->valueForKey("polylinePoints");
    std::string pointsStd = linePoints->m_sString;
    
    std::string pointEndStd = pointsStd;
    int posNextPoint = 0;
    
    while (true)
    {
        int newSpasePos = pointEndStd.find(" ");
        if (newSpasePos == -1)
        {
            return points;
        }
        posNextPoint += newSpasePos;
        pointEndStd = pointsStd.substr(posNextPoint + 1, pointsStd.length() - posNextPoint);
        
        int posDelim = pointEndStd.find(",");
        CCString *coordinateX = CCString::create(pointEndStd.substr(0, posDelim));
        
        CCString * coordinateY = CCString::create(pointEndStd.substr(posDelim + 1, pointEndStd.length() - posDelim));
        CCPoint end = ccp(coordinateX->floatValue(), coordinateY->floatValue());
        end = ccpMult(end,1/ CCDirector::sharedDirector()->getContentScaleFactor());
        points->addControlPoint(ccpAdd(startPos, ccp(end.x, -end.y)));
        posNextPoint++;
    }
    return points;
}

CCPoint MapHelper::positionWithProperties(CCDictionary* properties)
{
    CCPoint startPos = ccp(properties->valueForKey("x")->floatValue(), properties->valueForKey("y")->floatValue());
    startPos = ccpMult(startPos, 1/ CCDirector::sharedDirector()->getContentScaleFactor());
    return startPos;
}