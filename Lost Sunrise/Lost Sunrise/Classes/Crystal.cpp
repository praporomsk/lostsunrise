//
//  Crystal.cpp
//  Lost Sunrise
//
//  Created by Roman on 5/28/13.
//
//

#include "Crystal.h"
#include "Constants.h"
#include "MapHelper.h"

Crystal::Crystal()
{
    
}

Crystal::~Crystal()
{
    
}

bool Crystal::init(b2World *world, CCDictionary *properties)
{
    CCPoint pos = MapHelper::positionWithProperties(properties);
    if (CCSprite::initWithSpriteFrameName("Crystal.png"))
    {
        setOpacity(150);

        
        isActive = false;
        canCatch = true;
        setTag(ObjectTypeCrystal);
        setPosition(pos);
        
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        bodyDef.position.Set(pos.x/PTM_RATIO, pos.y/PTM_RATIO);
        
        body = world->CreateBody(&bodyDef);
        
        // Define another box shape for our dynamic body.
        b2CircleShape *circleShape = new b2CircleShape();
        circleShape->m_radius = ((boundingBox().size.width/2) / PTM_RATIO) * getScale();
        
        // Define the dynamic body fixture.
        b2FixtureDef fixtureDef;
        fixtureDef.shape = circleShape;
        fixtureDef.density = 1.0f;
        fixtureDef.friction = 0.01f;
        fixtureDef.restitution = 1.0f;
        fixtureDef.isSensor = true;
        fixtureDef.filter.categoryBits = CATEGORY_SCENERY_OBJECTS;
        fixtureDef.filter.maskBits = MASK_SCENERY_OBJECTS;
        
        delete circleShape;
        circleShape = NULL;
        
        body->CreateFixture(&fixtureDef);
        body->SetUserData(this);
        return true;
    }
    return false;
}

void Crystal::deActive()
{
    canCatch = false;
    isActive = false;
    setDisplayFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("Crystal.png"));
}

void Crystal::setIsActive()
{
    if (!canCatch) return;

    if (!isActive)
    {
        isActive = true;
        setDisplayFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("CrystalActive.png"));
    }
}