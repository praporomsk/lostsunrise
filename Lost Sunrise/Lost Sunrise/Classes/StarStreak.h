//
//  StarStreak.h
//  Lost Sunrise
//
//  Created by Roman on 7/21/13.
//
//

#ifndef __Lost_Sunrise__StarStreak__
#define __Lost_Sunrise__StarStreak__

#include "cocos2d.h"
#include "Box2D.h"

USING_NS_CC;

class StarStreak : public CCSprite
{
    
public:
    ~StarStreak();
    StarStreak();
    
    virtual bool init(b2World* world,float radius);
    void updateTarget(b2World *world);
    void setVisible(bool isVisible);
    
    float angle;
    b2Body *m_body;
    
    CCArray *m_targetPoints;
};
#endif /* defined(__Lost_Sunrise__StarStreak__) */
