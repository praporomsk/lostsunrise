//
//  GameOverLayer.cpp
//  Lost Sunrise
//
//  Created by Roman on 8/11/13.
//
//

#include "GameOverLayer.h"
#include "GameManager.h"

GameOverLayer::GameOverLayer(){}

GameOverLayer::~GameOverLayer(){}

bool GameOverLayer::init()
{
    if (CCLayer::init()) {
        
        CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
        
        m_background = CCSprite::create("Backgrounds/nigth.png");
        m_background->setAnchorPoint(ccp(0.0, 0.0));
        m_background->setPosition(ccp(0.0, 0.0));
        //background->cocos2d::CCNode::setPosition(screenSize.width/2, getRealHeight());
        addChild(m_background);
        
        m_moon = CCSprite::createWithSpriteFrameName("moon.png");
        m_moon->setAnchorPoint(ccp(0.5, 0.2));
        m_moon->setPosition(ccp(screenSize.width * 0.25, screenSize.height * 0.8));
        m_background->addChild(m_moon);
        
        return true;
    }
    return false;
}

void GameOverLayer::winAnimation()
{
    CCSprite *dayBacoground = CCSprite::create("Backgrounds/day.png");
    dayBacoground->setAnchorPoint(CCPointZero);
    dayBacoground->setPosition(CCPointZero);
    dayBacoground->setOpacity(0.0);
    m_background->addChild(dayBacoground);
    
    
    CCDelayTime *time = CCDelayTime::create(1.0f);
    CCFadeIn *fade = CCFadeIn::create(2.0f);
    CCCallFunc *showWinMenu = CCCallFunc::create(this, callfunc_selector(GameOverLayer::showWinMenu));
    dayBacoground->runAction(CCSequence::create(time, fade, showWinMenu, NULL));
    
    CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
    
    CCRotateBy *moonRotate = CCRotateBy::create(1.5, 180);
    CCEaseElasticOut *easeMoon = CCEaseElasticOut::create(moonRotate, 0.7);
    CCMoveBy *moonMove = CCMoveBy::create(3.0, ccp(0.0, -screenSize.height));
    m_moon->runAction(CCSequence::create(easeMoon, moonMove, NULL));
}

void GameOverLayer::showWinMenu()
{
    CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
    CCMenuItemSprite *levelItem = CCMenuItemSprite::create(CCSprite::create("Buttons/bt_levelMenu.png"),
                                                           CCSprite::create("Buttons/bt_levelMenu.png"),
                                                           this,
                                                           menu_selector(GameOverLayer::selectLevel));
    levelItem->setPosition(screenSize.width * 0.25, screenSize.height * 0.3);
    
    CCMenuItemSprite *restartItem = CCMenuItemSprite::create(CCSprite::create("Buttons/bt_restart.png"),
                                                             CCSprite::create("Buttons/bt_restart.png"),
                                                             this,
                                                             menu_selector(GameOverLayer::restartLevel));
    restartItem->setPosition(screenSize.width * 0.5, screenSize.height * 0.3);
    
    CCMenuItemSprite *nextLevelItem = CCMenuItemSprite::create(CCSprite::create("Buttons/bt_nextLevel.png"),
                                                               CCSprite::create("Buttons/bt_nextLevel.png"),
                                                               this,
                                                               menu_selector(GameOverLayer::nextLevel));
    nextLevelItem->setPosition(screenSize.width * 0.75, screenSize.height * 0.3);
    
    CCMenu *menu = CCMenu::create(levelItem, restartItem, nextLevelItem, NULL);
    menu->setPosition(CCPointZero);
    addChild(menu);
}

void GameOverLayer::showGameOverMenu()
{
    showStars();
    CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
    CCMenuItemSprite *levelItem = CCMenuItemSprite::create(CCSprite::create("Buttons/bt_levelMenu.png"),
                                                           CCSprite::create("Buttons/bt_levelMenu.png"),
                                                           this,
                                                           menu_selector(GameOverLayer::selectLevel));
    levelItem->setPosition(screenSize.width * 0.3, screenSize.height * 0.3);
    
    CCMenuItemSprite *restartItem = CCMenuItemSprite::create(CCSprite::create("Buttons/bt_restart.png"),
                                                             CCSprite::create("Buttons/bt_restart.png"),
                                                             this,
                                                             menu_selector(GameOverLayer::restartLevel));
    restartItem->setPosition(screenSize.width * 0.7, screenSize.height * 0.3);
    
    CCMenu *menu = CCMenu::create(levelItem, restartItem, NULL);
    menu->setPosition(CCPointZero);
    addChild(menu);
}

void GameOverLayer::showStars()
{
    for (int i = 0; i < 5; i++)
    {
        CCString *starName = CCString::createWithFormat("Backgrounds/stars/star_%02d.png",i);
        CCSprite *star = CCSprite::create(starName->getCString());
        star->setScale(0);
        //star->setOpacity(0);
        star->setPosition(starPos(i));
        m_background->addChild(star);
        
        float t = (float)i / 2;
        CCLog("t=%f",t);
        CCDelayTime *time = CCDelayTime::create(t);
        CCScaleTo *scale = CCScaleTo::create(0.7, 1.0);
        CCEaseBackOut *ease = CCEaseBackOut::create(scale);
        star->runAction(CCSequence::createWithTwoActions(time, ease));
    }
}

CCPoint GameOverLayer::starPos(int number)
{
    if (number == 0) {
        return ccp(428 / 2, (2048 - 486 - 64) /2);
    }else if (number == 1){
        return ccp(1380 / 2, (2048 - 96 - 32) / 2);
    }else if (number == 2){
        return ccp(1334.5 / 2, (2048 - 162 - 74) / 2);
    }else if (number == 3){
        return ccp(1189 / 2, (2048 - 196 ) / 2);
    }else if (number == 4){
        return ccp(272 / 2, (2048 - 696) / 2);
    }
    return CCPointZero;
}

#pragma mark -
#pragma mark selectors
void GameOverLayer::selectLevel(CCMenuItem *sender)
{
    sender->setEnabled(false);
    GameManager::sharedGameManager()->launchSelectLevel();
}

void GameOverLayer::restartLevel(CCMenuItem *sender)
{
    sender->setEnabled(false);
    GameManager::sharedGameManager()->restartLevel();
}

void GameOverLayer::nextLevel(CCMenuItem *sender)
{
    sender->setEnabled(false);
    GameManager::sharedGameManager()->nextLevel();
}