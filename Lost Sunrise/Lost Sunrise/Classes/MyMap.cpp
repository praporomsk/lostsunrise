//
//  MyMap.cpp
//  Lost Sunrise
//
//  Created by Roman on 5/30/13.
//
//

#include "MyMap.h"

MyMap::MyMap()
{
    
}

MyMap::~MyMap()
{
    
}

bool MyMap::initWithTMXFile(const char *tmxFile)
{
    if (CCTMXTiledMap::initWithTMXFile(tmxFile))
    {
        mapMode = MAP_MODE_FOLLOW;
        speed = 0.0f;
        
        scheduleUpdate();
        return true;
    }
    return false;
}

float MyMap::getRealHeight()
{
    return boundingBox().size.height - getTileSize().height * 2;
}

void MyMap::setPosition(const cocos2d::CCPoint &position)
{
    //float mapYposition = position.y;
    float mapYposition = MIN(0.0, position.y);
    mapYposition = MAX(-boundingBox().size.height + getTileSize().height * 2, mapYposition);
    CCTMXTiledMap::setPosition(ccp(position.x, mapYposition));
}

void MyMap::update(float delta)
{
    if (mapMode == MAP_MODE_SCROLL_TO_STOP)
    {
        speed *= 0.9;
        setPosition(ccp(getPosition().x,  getPosition().y + speed));
        if (speed < 0.1 && speed > -0.1) mapMode = MAP_MODE_FOLLOW; 
    }else if (mapMode == MAP_MODE_FOLLOW){
        CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
        float needPos = (screenSize.height / 2) - sunPosition.y;
        needPos = MIN(0.0, needPos);
        float dy = needPos - getPositionY();

        setPositionY(getPositionY() + dy / 17);
    }
}
